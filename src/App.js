import React from 'react'
import { Route, Switch } from 'react-router'
import HomeScreen from './containers/Home.screen'
import CartScreen from './containers/Cart.screen'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'

function App() {
  return (
    <Switch>
      <Route exact path="/" component={HomeScreen} />
      <Route path="/cart" component={CartScreen} />
    </Switch>
  )
}

export default App
