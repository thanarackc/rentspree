const initialState = {
  items: []
}

const products = (state = initialState, action) => {
  switch (action.type) {
    case 'PRODUCT_REQUEST':
      return initialState
    case 'PRODUCT_FAILURE':
      return action.data
    default:
      return initialState
  }
}

export default products
