const initialState = {
  products: [],
  quantityById: {}
}

const cart = (state = initialState, action) => {
  switch (action.type) {
    case 'CHECKOUT_REQUEST':
      return initialState
    case 'CHECKOUT_FAILURE':
      return action.cart
    default:
      return {
        products: [],
        quantityById: {}
      }
  }
}

export default cart
