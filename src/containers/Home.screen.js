import React from 'react'
import { Col, Container, Row } from 'reactstrap'
import NavbarComponent from '../components/Navbar'
import styled from 'styled-components'

const HomeWrap = styled.div`
  margin-top: 10px;
`

const HeadTitle = styled.h1`
  font-size: 24px;
`

const HomeScreen = () => {
  return (
    <React.Fragment>
      <NavbarComponent />
      <HomeWrap>
        <Container>
          <HeadTitle>Products List</HeadTitle>
        </Container>
      </HomeWrap>
    </React.Fragment>
  )
}

export default HomeScreen
