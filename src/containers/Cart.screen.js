import React from 'react'
import { Col, Container, Row } from 'reactstrap'
import NavbarComponent from '../components/Navbar'
import styled from 'styled-components'

const CartWrap = styled.div`
  margin-top: 10px;
`

const CartScreen = () => {
  return (
    <React.Fragment>
      <NavbarComponent />
      <CartWrap>
        <Container>
          <Row>
            <Col>Cart</Col>
          </Row>
        </Container>
      </CartWrap>
    </React.Fragment>
  )
}

export default CartScreen
