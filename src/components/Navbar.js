import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap'

const NavbarComponent = props => {
  const [isOpen, setIsOpen] = useState(false)

  const toggle = () => setIsOpen(!isOpen)

  const logoUrl =
    'https://mk0rentspree9nsuonti.kinstacdn.com/wp-content/uploads/sites/2/2019/06/rentspree-logo-gradient-1.svg'

  return (
    <div>
      <Navbar light className="navbar-custom" expand="md">
        <Link className="navbar-brand" to="/">
          <img src={logoUrl} />
        </Link>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <Link className="nav-link" to="/">
                Home
              </Link>
            </NavItem>
            <NavItem>
              <Link className="nav-link" to="/cart">
                Cart
              </Link>
            </NavItem>
          </Nav>
          <NavbarText className="item-count">Items (0)</NavbarText>
        </Collapse>
      </Navbar>
    </div>
  )
}

export default NavbarComponent
